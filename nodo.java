//NOMBRE : JOSE ALEJANDRO COLQUE CALLAHUARA
//R.U:76542
//C.I:8598092
package com.mycompany.repasolistas;
public class nodo<T> {
    private T value;
    private nodo<T> next;
    public nodo(T v, nodo<T> n){
        this.value = v;
        this.next = n;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public nodo<T> getNext() {
        return next;
    }

    public void setNext(nodo<T> next) {
        this.next = next;
    }
    
}
